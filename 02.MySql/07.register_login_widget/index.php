<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
	    <meta charset="UTF-8">
		<meta name="author" content="abu based">
	    <meta name="keyword" content="html,css,javascript,jqurey,fast-webpage,fast-website">
	    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
	    <title>Register and Login</title>
		<!-- <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">  -->

		<!--web-fonts-->
		<link href='//fonts.googleapis.com/css?family=Jura:400,300,500,600' rel='stylesheet' type='text/css'>
		 <!-- Main css -->
		 <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="style.css">
		<!-- Responsive css -->
		<link rel="stylesheet" href="responsive.css">

	</head>

    <body>
	    <h1>Register Login Widget</h1>
	    <div class="box">
		   <div class="divone">
		    <h2>Login</h2>
		      <form method="POST" action="login.php">
			      <input type="text" name="Email" placeholder="E-mail" required>
				  <input type="password" name="password" placeholder="password" required>
				  <input type="submit" value="LOGIN NOW" required>
			  </form>
			  <div class="link">
			     <a href="#" class="Facebook">Facebook</a>
				 <a href="#" class="Google">Google+</a>
			  </div>

			  <p><a href="#">Don't have an account?</a></p>
		   </div>
		   <div class="divtwo">
		       <h2>Register<h2>
		       <form method="POST" action="<?php echo /* htmlspecialchars($_SERVER['PHP_SELF']) */ "register.php"; ?>">
			       <input type="email" name="email" placeholder="E-mail">
				   <input type="text" name="username" placeholder="Username">
				   <input type="password" name="password" placeholder="password">
				   <input type="password" name="confirm_password" placeholder="Confirm Password">
				   <input type="submit" name="submit_register" value="REGISTER">
				   </form>
				   <p><a href="#">By clicking register, I agree to your terms</a></p>
			</div>
		</div>
		<div class="clear"></div>
		<div class="footer">
		    <p>© 2016 Register Login Widget . All rights reserved | Design by W3layouts</p>
		</div>
		<script src="../../assets/js/jquery-3.5.1.js"></script>
    	<script src="../../assets/js/bootstrap.bundle.min.js"></script>
		<script src="../../assets/js/sweetalert.min.js"></script>
		<?php if(!empty($_SESSION['error'])){
			?>
			<script>
				let error = document.createElement("DIV");
				error.innerHTML = '<?php echo $_SESSION['error'];?>';
				swal({
					content:error,
					button:false,
					timer:3000,
					icon:"warning",
					className:"alert",
					className:"alert-warning",
				});
			</script>
			<?php
			// destroy the session
			session_destroy();
		};
		?>
	</body>
</html>

