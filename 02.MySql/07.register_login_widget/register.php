<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] = "POST") {
    if (isset($_POST['submit_register'])) {

		$email=$_POST['email'];
		$username=$_POST['username'];
		$password=$_POST['password'];
		$confirm_password=$_POST['confirm_password'];
		$_SESSION['error'] = NULL;
		$token = bin2hex(random_bytes(15));

		if (empty($email)) {
			$_SESSION['error'] .= "<p>Email field is required</p>";
		}else{
			$email = filter_var($email,FILTER_SANITIZE_EMAIL);
			if(!filter_var($email,FILTER_SANITIZE_EMAIL)){
				$_SESSION['error'] .= "<p>Email is not valid.</p>";
			}
		};
		if (empty($username)) {
			$_SESSION['error'] .= "<p>Username field is required</p>";
		} else {
			$username = filter_var($username, FILTER_SANITIZE_STRING);
		};
		if (empty($password)) {
			$_SESSION['error'] .= "<p>Password field is required</p>";
		} else {
			$hash_password= password_hash($password,PASSWORD_BCRYPT);
		};
		if($password!=$confirm_password){
			$_SESSION['error'] .= "<p>Password doesn't match</p>";
		};
		if(empty($_SESSION['error'])){
			$mysql = new mysqli;
			$mysql->connect('localhost','root','','learningphp');
			if ($mysql->connect_error) {
				?>
				<script>
					window.alert('Connect to Database Error ' +'<?php echo($mysql->connect_errno); ?>');
				</script>
			<?php
			}
			 $checkEmail = $mysql->query("SELECT email FROM signup WHERE email='$email'");
			// var_dump($checkEmail);die();
			if ($checkEmail->num_rows>0) {
				$_SESSION['error'] .= "<p>Email is allready exist</p>";
			} else {
				$query = "INSERT INTO signup(email,username,password,token) VALUES('$email','$username','$hash_password','$token')";
                $mysql->query($query);
                $_SESSION['user'] = $mysql->insert_id;
                header("Location:home.php");
				if($mysql->error){
					?>
					<script>
						// window.alert("Registration Failed");
						swal({
							title:"Opps!",
							text:"Registration Failed",
							icon:"error",
							button:false,
							timer:3000,
							className:"alert",
							className:"alert-danger",
						});
					</script>
                <?php
                header("Location:index.php");
				}
			}	
		}else{
            header("Location:index.php");
        }
    }
}
?>
