<?php session_start();
// $user = NULL;
if(isset($_SESSION['user'])){
    $userId = $_SESSION['user'];
}else{
    header("Location:index.php");
}
$mysql = new mysqli;
$mysql->connect('localhost','root','','learningphp');
if ($mysql->connect_error) {
    ?>
    <script>
        window.alert('Connect to Database Error ' +'<?php echo($mysql->connect_errno); ?>');
    </script>
<?php
}
$result = $mysql->query("SELECT * FROM signup WHERE id='$userId'");
$user = $result->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <title>My Profile</title>
    <style>
        body{
            font-family: 'PT Sans Narrow', sans-serif;
            font-size: 16px;
            background-color: #2a2c30;
        }
        .profile{
            padding: 33px 33px 22px;
            background:#3a3b40;
            margin:50px 0;
            color:#eeeeee;
        }
        #name{
            color:#f6416c;
        }
        h1,button{text-transform:uppercase;color:#fff;}
        h5{color:#aaaaaa;}
        .details ul li span:first-child{
            color:#aaaaaa;
            padding-right:5px;
            width:30%
        }
        .details ul li{
            list-style:none;
            display:flex;
        }
        .sectionOne{
            display: flex;
            flex-direction: column;
        }
        .social a{
            display:inline-block;
            border: 1px solid #aaaaaa;
            width: 34px;
            height: 34px;
            line-height: 34px;
            text-align: center;
            transition: all .2s;
            color: #aaaaaa;
            margin: 5px;
        }
        .social a:hover{
            background:#f6416c;
            border:none;
        }
        button,.logout{
            background:#3a3b40;
            border:none;
            box-shadow:0px 0px 10px #f6416c;
            font-size:1rem !important;
            color:#aaaaaa;
        }
        .logout{float:right;}
        .logout:hover,button:hover{color:#f6416c;box-shadow:0px 0px 10px #aaa;}
    </style>
</head>
<body>
    <div class="container">
        <div class="row profile">
            <section class="sectionOne col-sm-4">
                <img class="img-fluid" src="images/profile.jpg" width="100%">
                <button class="my-4 btn btn-lg"><i class="m-2 d-inline-block fas fa-cloud-download-alt"></i>Download resume</button>
                <span class="text-center" style="color:#f6416c"><i class=" d-inline-block mx-2 fa fa-check" aria-hidden="true"></i>available on freelance</span>
            </section>
            <section class="sectionTwo col-sm-8">
                <h1>Hi i'm <span id="name"><?= $user['username']?></span></h1>
                <h5>web developer & web designer</h5>
                <p>Hello! I’m Robert Smith. Web Developer with over 8 years of experience. Experienced with all stages of the development cycle for dynamic web projects. Having an in-depth knowledge including advanced HTML5, CSS3, JavaScript, jQuery, Angular JS. Strong background in management and leadership.</p>
                <div class="row details">
                    <ul class="col-6">
                        <li><span>Age</span><span>25</span></li>
                        <li><span>Address</span><span>5420,Gobindanganj,Gaibandha</span></li>
                        <li><span>Email</span><span><?= $user['email']?></span></li>
                    </ul>
                
                    <ul class="col-6">
                        <li><span>Phone</span><span>01797944174</span></li>
                        <li><span>website</span><span>www.weblancerbd.com</span></li>
                        <li><span>Nationality</span><span>Bangladeshi</span></li>
                    </ul>
                </div>
                <p>Social Links</p>
                <div class="social">
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                </div>
                <a href="logout.php" class="logout btn btn-sm">Logout</a>
            </section>
        </div>
    </div>
    <script src="assets/js/jquery-3.5.1.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/sweetalert.min.js"></script>
</body>
</html>
