<?php
define("Title","MySQL Connect To Database");
include '../../assets/header.php';
include '../../assets/headerbottom.php';
echo '<h2 class="text-center"> '.Title.' :</h2>';?>
        
<!-- You can start from here -->


<?php 

$mysql = new mysqli($host = 'localhost',$username = 'root', $password = '');

if($mysql->connect_error){
    die("<h3 class='alert alert-warning'>Unable to connect database error: ".$mysql->connect_error."</h3>");
}else{
    echo "<h3 class='alert alert-success'>Connect to Database Successfully</h3>";
    // var_dump($mysql);
}
echo "<h1 class='text-center'>Create a Database</h1><hr>";

$sql = "CREATE DATABASE testDb";
$check = $mysql->query($sql);

if($check){
    echo "<h3 class='alert alert-success'>Database Create Successfully</h3>";
}else{
    echo "<h3 class='alert alert-warning'>Unable to Create Database due to ".$mysql->error."</h3>";
}

    // সংযোগ বিচ্ছিন
    $mysql->close();
?>

<?php include '../../assets/footer.php';
