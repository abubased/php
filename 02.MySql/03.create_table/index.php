<?php
define("Title", "MySQL Create Table");
include '../../assets/header.php';
include '../../assets/headerbottom.php';
echo '<h2 class="text-center"> ' . Title . ' :</h2>';?>

<!-- You can start from here -->


<?php

$mysql = new mysqli($host = 'localhost', $username = 'root', $password = '', $database = 'testDb');

if ($mysql->connect_error) {
    die("<h3 class='alert alert-warning'>Unable to connect database error: " . $mysql->connect_error . "</h3>");
} else {
    echo "<h3 class='alert alert-success'>Connect to Database Successfully</h3>";
    // var_dump($mysql);
}
echo "<h1 class='text-center'>Create Table</h1><hr>";

// Delete a Table if already exits
// $sql = "DROP TABLE user";
// $mysql->query($sql);

$sql = "CREATE TABLE user(
    Id INT(6)  UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    role VARCHAR(20) DEFAULT 'user',
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    create_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)";

$check = $mysql->query($sql);

if ($check) {
    echo "<h3 class='alert alert-success'>Table Create Successfully</h3>";
} else {
    echo "<h3 class='alert alert-warning'>Unable to Create Table due to " . $mysql->error . "</h3>";
}

// সংযোগ বিচ্ছিন
$mysql->close();
?>

<?php include '../../assets/footer.php';
