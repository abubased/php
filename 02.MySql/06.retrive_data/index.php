<?php define('Title', 'MySQL Retrive Data From Database');
include "../../assets/header.php";
include "../../assets/headerBottom.php";?>
            <h1 class="text-center"><?=Title?></h1><hr>

<?php
    $mysql = new mysqli('localhost', 'root', '', 'testdb');
    $sql = "SELECT * FROM user";
    $result = $mysql->query($sql) or die("Unable to Fetch Data from Database".$mysql->error);
    if ($result->num_rows > 0) {
        // var_dump($result);
        ?>
        <table class="table table-bordered table-striped">
            <thead class="thead-dark">
                <th scope="col">Serial NO</th>  
                <th scope="col">Username</th>  
                <th scope="col">Email</th>  
                <th scope="col">Password</th>  
            </thead>
            <tbody>
            <?php
                $i=0;
                while($row = $result->fetch_assoc()){
                    $i++;
                    echo "<tr>
                            <th scope='row'>".$i."</th>
                            <td>".$row['username']."</td>
                            <td>".$row['email']."</td>
                            <td>".$row['password']."</td>
                    </tr>
                    ";
                }
            ?>
            </tbody>
        </table>

    <?php }else{
        echo "There is no data in this table";
    }
    
?>

<?php include "../../assets/footer.php"; ?>
