<?php define('Title', 'MySQL inser data using a form');
include "../../assets/header.php";
include "../../assets/headerBottom.php";?>
            <h1 class="text-center"><?=Title?></h1><hr>
        <div>
<?php
    if ($_SERVER['REQUEST_METHOD'] == "POST"){
        $username = $_REQUEST['username'];
        $email = $_REQUEST['email'];
        $password = $_REQUEST['password'];
        $error_message = NULL;

        ($username==NULL) ? $error_message .= "<p>Username can not be empty</p>\r\n" :$username = filter_var($username, FILTER_SANITIZE_STRING);

        ($email==NULL) ? $error_message .= "<p>Email field can not be empty</p>\r\n" : (filter_var($email, FILTER_SANITIZE_EMAIL))?$email=filter_var($email,FILTER_VALIDATE_EMAIL):$error_message .= "<p>Please type a valid Email</p>\r\n";

        ($password==NULL)? $error_message .= "<P>Password can't be empty</P>\r\n": $password;

        if(isset($error_message)){
            $result_message=$error_message;
            ?>
        <div class="alert alert-warning">
            <?=$result_message."\r\n"?>
        </div>
        <?php
        }
        else{
            $mysql = new mysqli('localhost','root','','testdb');
            $username = $mysql->real_escape_string($username);
            $email = $mysql->real_escape_string($email);
            $password = $mysql->real_escape_string($password);
            $password = md5($password);
            $sql = "INSERT INTO user(username,email,password) VALUES('$username', '$email', '$password')";
            
            if($mysql->query($sql)){
                $result_message="<p>Data Insert Successfully</p>";
                ?><div class="alert alert-success">
                    <?=$result_message?>
                </div><?php
            }
            else{
                $result_message="<p>Something went wrong try again later</p>";
                ?><div class="alert alert-warning">
                    <?=$result_message?>
                </div><?php
            }
        };
    };
?>
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
            <div class="form-group">
                <label for="username">User Name:</label>
                <input type="text" name="username" id="username" class="form-control">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" name="email" id="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" id="password" class="form-control">
            </div>
            <button type="submit" class="btn-outline-success">Submit</button>
        </form>
    </div>
<?php include "../../assets/footer.php"; ?>
