<?php
define("Title","MySQL Connect To Database");
include '../../assets/header.php';
include '../../assets/headerbottom.php';
echo '<h2 class="text-center"> '.Title.' :</h2>';?>
        
<!-- You can start from here -->


<?php 

// MySQLi প্রসিডিউরাল(Procedural) প্রক্রিয়ায় সংযোগ তৈরি
// $link = @mysqli_connect($host = 'localahost', $user = 'root', $password = '', $database = 'learningphp') or die("Unable to connect database error: ".mysqli_connect_error());
// echo "<pre>";
// var_dump($link);


// সংযোগ বিচ্ছিন
//mysqli_close($link);
// MySQLi অব্জেক্ট-অরিয়েন্টেড প্রক্রিয়ায় সংযোগ তৈরি

$mysql = new mysqli($host = 'localhost',$username = 'root', $password = '');

if($mysql->connect_errno){
    die("<h3 class='alert alert-warning'>Unable to connect database error: ".$mysql->connect_error."</h3>");
}else{
    echo "<h3 class='alert alert-success'>Connect to Database Successfully</h3>";
    var_dump($mysql);

    // সংযোগ বিচ্ছিন
    $mysql->close();
}

?>

<?php include '../../assets/footer.php';
