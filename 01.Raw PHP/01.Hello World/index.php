<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta author="Md Abu Based">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hello World</title>
</head>
<body>
    <p>পিএইচপি লিখার জন্য আপনি যে ফাইলটি নিবেন তার extension অব্যশই .php  হতে হবে যেমনঃ index.php ।
    তারপর &lt;?php পিএইচপি কোড শুরু করতে হবে  এবং ?&gt; দিয়ে পিএইচপি কোড শেষ করতে হবে ।
    উদাহরণঃ<br></p>
    <pre>  
        &lt;?php
            echo 'hello world'&#59;
        ?&gt;
        ফলাফলঃ
    </pre>
    <?php
        echo "hello World";
    ?>
    <br><br><hr><center>Copyright by &copy; Md Abu Based</center>
</body>
</html>
