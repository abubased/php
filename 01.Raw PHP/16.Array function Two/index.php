<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GET & POST</title>
</head>
<body>
    <h2>Array Function:</h2>

    <?php
        $fruits = array('mango','apple','banana','jack-fruits','Carambola');
        print_r($fruits);
        echo '<p>After Array Splice</p>';
        array_splice($fruits,0,2,array('Cucumber','Orange','kiwi','lemon','Dead fruit'));
        print_r($fruits);

        echo '<p>After Sorting assending order</p>';
        sort($fruits);
        print_r($fruits);
        echo '<p>After Sorting dessending order</p>';
        rsort($fruits);
        print_r($fruits);

        $vegetables = array('Asparagus'=>'Loti','Beans'=>'Sim','Broccoli'=>'Fullcopy','Eggplant'=>'Begun');
        echo '<p>After Sorting assending order by values</p>';
        asort($vegetables);
        print_r($vegetables);

        echo '<p>After Sorting assending order by keys</p>';
        ksort($vegetables);
        print_r($vegetables);
    ?>

        <br><br><hr><center>Copyright by &copy; Md Abu Based</center>
</body>
</html>
