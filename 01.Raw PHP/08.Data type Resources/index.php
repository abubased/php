<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Type Resources</title>
</head>
<body>
    <h2>Resources:</h2>
    <?php
        $myfile = fopen('sometext.text','r');
        var_dump($myfile);
        echo '<br>';
        // echo fread($myfile,20);
        echo fread($myfile,filesize('sometext.text')); 
    ?>
</body>
</html>
