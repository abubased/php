<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>For Loop</title>
</head>
<body>
    <h2>While loop:</h2>
    <?php
        $i = 1;
        while($i <= 10){
            echo $i.'<br>';
            $i++;
        }
        $webdesign = array('HTML','CSS','Javascript');
        $a = array_key_last($webdesign);
        $i = 0;
        while($i<=$a){
            echo $webdesign[$i].'<br>';
            $i++;
        }
    ?>

</body>
</html>
