<?php define("Title","date time and mktime");
include '../../assets/header.php';
include '../../assets/headerbottom.php';

echo '<h2 class="text-center">'. Title .':</h2>';?>
            <!-- your code will be start from here -->
            <h2 class="text-center">Date() time() and mktime():</h2>
            <?php

/*****************************************************************
 ****************************************************************
                    Formatting the Dates and Times with PHP
                     Day of the month:
                        d (01/31)->two digits with leading zeros
                        j (1/31)->without leading zeros
                     Day of the week:
                        D (Mon/Sun)->text as an abbreviation
                        l (monday)->full lowercase
                        L (MONDAY)->full uppercase
                     Month:
                        m (01/12)->two digits with leading zeros
                        M (Jan)->text as an abbreviation
                        F (January)-> Full month
                     Year:
                        y (09/15)->two digits
                        Y (2009-2015)->Four Digits

                     Separators:
                        hyphens: (-)
                        dots: (.)
                        slashes: (/)
                        spaces: ( )
********************************************************************
********************************************************************/

                echo '<p><strong><i>Date using date() function</stron</strong>i>g>strong></p>';
                $today = date("d F Y");
                echo 'today is '.$today;


/***********************************************************************
 * *********************************************************************
                    format the time string:
                        hour:
                            h (01-12)->12-hour format with leading zeros
                            H (00-24)->24-hour format with leading zeros
                        minutes:
                            i (01-59)->minutes with leading zeros
                        seconds:
                            s (01-59) ->seconds with leading zeros
                        Ante meridiem and Post meridiem
                            a->lowercase
                            A->uppercase
******************************************************************
*****************************************************************/

                echo '<p><strong><i>Time using date() function</i></strong></p>';
                $time = date('h:m:s A');
                echo 'Current time is now '.$time;

                echo '<p><strong><i>Current timestamp using time() function:</i></strong></p>';
                echo 'timestamp is now '.time();

                echo '<p><strong><i>Convert timestamp to time:</i></strong></p>';
                $timestamp = time();
                $time = date('D/F/y h:m:s a',$timestamp);
                echo $time;

                        //mktime(hour,minute,second,month,day,year)
                echo '<p><strong><i>Convert time to timestamp using mktime() function:</i></strong></p>';
                $timestamp = mktime('10','32','56','6','17',date('Y'));
                echo "10:32:56am in 17th june 2020 to timestamp = ".$timestamp;

                echo '<p><strong><i>Find what day You were bron</i></strong></p>';
                $timestamp = mktime('0','0','0','9','27','1997');
                $birthdate = date('l',$timestamp);
                echo "I were bron in ".$birthdate;
            ?>
    
    <!-- this will be include a file on directory assets/footer.php -->
    <?php include '../../assets/footer.php'; ?>
