<?php define("Title","Filter Clean User Inputs");
include '../../assets/header.php';
include '../../assets/headerbottom.php';

echo '<h2 class="text-center">'. Title .':</h2>';?>
        
<!-- You can start from here -->
    <?php
        //username example
        $myUsername = '<script>window.alert("Hi")</script>';
        $myUsername = filter_var($myUsername, FILTER_SANITIZE_STRING);
        echo $myUsername;
        //email example
        $myEmail = 'abu     based  \\\\@    outlook.com';
        $myEmail = filter_var($myEmail, FILTER_SANITIZE_EMAIL);
        echo "<br />" . $myEmail;
        //URL example
        $myURL = "http://££www.    google.com";
        $myURL = filter_var($myURL, FILTER_SANITIZE_URL);
        echo "<br />" . $myURL;
    ?>

    <h3>Validate user inputs</h3>
    
    <?php
        //Email validation
        $myEmail = 'abu     based  \\\\@    outlook.com';
        $myEmail = filter_var($myEmail, FILTER_SANITIZE_EMAIL);
        echo "<p>Cleaned email: $myEmail</p>";
        echo "<p>Email validation: " . filter_var($myEmail, FILTER_VALIDATE_EMAIL) . "</p>";
        if(filter_var($myEmail, FILTER_VALIDATE_EMAIL)){
            echo "<p>Valid Email</p>";   
        }else{
            echo "<p>Invalid Email</p>";   
        }
        //URL validation
        $myURL = "http://££www.    google.com";
        $myURL = filter_var($myURL, FILTER_SANITIZE_URL);
        echo "<p>Cleaned URL: $myURL</p>";
        echo "<p>URL validation: ". filter_var($myURL, FILTER_VALIDATE_URL) ."</p>"
    ?>

<?php include '../../assets/footer.php';


