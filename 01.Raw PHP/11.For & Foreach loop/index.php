<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>For Loop</title>
</head>
<body>
    <h2>For loop:</h2>
    <?php
        for($i = 1; $i <= 10; $i++){
            echo $i."<br>";
        }
    ?>
    <h2>Foreach loop:</h2>
    <?php
        $webdesign = array('HTML','CSS','Javascript');
        foreach ($webdesign as $value) {
            echo $value.'<br>';
        }
        $webdesign = array('HTML' => 'Hyper Text Markup Language','CSS' => 'cascading style sheet','Javascript' => 'Scripting Language');
        foreach ($webdesign as $key => $value) {
            echo $key.' = '.$value.'<br>';
        }
    ?>
</body>
</html>
