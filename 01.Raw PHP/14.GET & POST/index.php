<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GET & POST</title>
</head>
<body>
    <h2>GET:</h2>
    <form method="get">
        <input type="text" name="search" placeholder="Type Keywords">
        <button type="submit">Submit</button>
    </form>
    <h2>POST:</h2>
    <form method="post">
        <input type="text" name="name" placeholder="Full Name">
        <button type="submit">Submit</button>
    </form>
    
    <?php
        if($_GET){
            if($_GET['search'] and !empty($_GET['search'])){
                print_r($_GET);
                echo "<P>Your are Searching for ".$_GET['search']."</P>";
            }else{
                echo "<p>Please Type Something in the input box!</p>";
            }
        }else{
            if($_POST['name'] and !empty($_POST['name'])){
                print_r($_POST);
                echo "<p>Hey, ".$_POST['name']." thanks for visit here.</p>";
            }else{
                echo "<p>Please Type Something in the input box!</p>";
            }
        }
    ?>





<br><br><hr><center>Copyright by &copy; Md Abu Based</center>
</body>
</html>
