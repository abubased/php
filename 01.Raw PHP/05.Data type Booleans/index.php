<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Type Booeans</title>
</head>
<body>
    <h2>Booleans:</h2>
    <?php
        $abooleansvariable = (10<8);
        
        var_dump($abooleansvariable);
        echo "<br>";

        $abooleansvariable2 = (8<10);
        var_dump($abooleansvariable2);

        $abooleansvariable3 = $abooleansvariable && $abooleansvariable2;
        var_dump($abooleansvariable3);

        $abooleansvariable4 = $abooleansvariable || $abooleansvariable2;
        var_dump($abooleansvariable4);

        // Print oposite of a variable
        var_dump(!$abooleansvariable3);

    ?>
</body>
</html>
