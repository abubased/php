<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta author="Md Abu Based">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP variabe & Constant</title>
</head>
<body>
    <h2>PHP Variable</h2>

        <p>ডলার সাইন &#34;&dollar;&#34; দিয়ে পিএইচপি তে ভেরিয়েবল ডিফাইন করা হয়,ডলার সাইন &#34;$&#34; এর সাথে ভেরিয়েবল এর নাম তারপর সমান চিহ্ন তারপর ভেরিয়েবলের ভ্যালু বা মান ।<br>
        উদাহরনঃ</p>
        <pre>
        &lt;?php 
        &dollar;name = 'Abu Based'&#59;
        &dollar;age = 27&#59;
        echo 'Hello, I am '.&dollar;name.'&lt;br&gt;'&#59;
        echo 'I am &dollar;age years old'&#59;
        ?&gt;
        ফলাফলঃ
        </pre>
    <?php
        $name = "Abu Based";
        $age = 27;
        echo "Hello, I am ".$name."<br>";
        echo "I am $age years old";
    ?>
    <h2>PHP Constant</h2>
        <p>define() মেথডটি দিয়ে পিএইচপি তে কন্স্ট্যান্ট ডিফাইন করা হয়, এর দুটী প্যারামিটার থাকে প্রথমটি কন্সট্যান্ট এর নাম এবং পরেরটি ভ্যালু । [ কন্সট্যান্ট এর নাম বড় হাতের অক্ষরে লিখতে হবে <br>
        উদাহরনঃ</p>
        <pre>
        &lt;?php
        define('COUNTRY','Bangladesh')&#59;
        echo 'I am live in '.COUNTRY&#59;
        ?&gt;
        ফলাফলঃ
        </pre>
    <?php        
        define('COUNTRY','Bangladesh');
        echo "I am live in ".COUNTRY;
    ?>
    <br><br><hr><em><strong>NOTE:</strong> লক্ষ্য করুন উপরের উদাহরন গুলিতে ভেরিয়েবল এবং স্ট্রিং কে একত্র করতে <strong>&nbsp; .</strong> ব্যাবহার করা হয়েছে । জাভাস্ক্রিপ্টে <strong>&nbsp; +</strong> ব্যাবহার করা হয়ে থাকে </em><hr>
    <a href="superGlobalVariable.php">PHP Super Global Variable</a>









    <br><br><hr><center>Copyright by &copy; Md Abu Based</center>
</body>
</html>
