<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GET & POST</title>
</head>
<body>
    <h2>Array Function:</h2>

    <?php
        $obligatory = array('fazar','jahar','asar','magrib','asa');
        $sunnat = array('fazar','jahar','magrib','asa');
        $prayers = array_merge($obligatory,$sunnat);
        print_r($prayers) ;
        echo '<br>'.count($obligatory);
        echo '<br>'.count($prayers).'<br>';
        $count = array_count_values($prayers);
        print_r($count);
        echo '<br>'.$count['asar'].'<br>';
        if(!in_array('asar',$sunnat)){
            echo 'There is no sunnat prayer in asar  <br>';
        }else{
            echo 'There is sunna <br>';
        };
        array_push($prayers,'betar');
        print_r($prayers);



    ?>

    <br><br><hr><center>Copyright by &copy; Md Abu Based</center>
</body>
</html>
