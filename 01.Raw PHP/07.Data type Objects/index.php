<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Type Booeans</title>
</head>
<body>
    <h2>Objects:</h2>
    <?php
        class car {
            //properties
            public $make = 'Ford';
            private $status = 'off';

            //methods
            public function trun_on()
            {
                $this->status = 'on';
            }
            public function getStatus()
            {
                return $this->status;
            }
        }

        $mycar = new car;
        var_dump($mycar);
        var_dump($mycar->make);
        // this will be go make an error
        // because we can't access the privet properties
        // var_dump($mycar->status);
        //but we can access this via method()

        // $mycar->trun_on();
        var_dump($mycar->getStatus());
    ?>
    
</body>
</html>
