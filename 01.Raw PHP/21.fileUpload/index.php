<?php define("Title","Upload Image");
include '../../assets/header.php';
include '../../assets/headerbottom.php';

echo '<h2 class="text-center">'. Title .':</h2>';?>
        
<!-- You can start from here -->
    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
        <div class="form-group">
            <label for="profile_photo">Choose Profile Picture:</label>
            <input type="file" class="form-control-file" name="profile_photo" id="profile_photo">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <?php
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            if(!$_FILES['profile_photo']['tmp_name']){
                echo "please! choose an Image";
            }else{
                $file_destination = 'file/';
                $file_directory = $file_destination.basename($_FILES['profile_photo']['name']);
                $fileUpload = true;
                $file_extension = pathinfo($file_directory,PATHINFO_EXTENSION);
                $check = getimagesize($_FILES['profile_photo']['tmp_name']);
                if($check !== false){
                    echo "ইমেজ ফাইলটি- " . $check['mime'] . ".<br>";
                    $fileUpload = true;
                }else{
                    echo "ফাইলটি ইমেজ নয়।";
                    $fileUpload = false;
                }
                if(file_exists($file_directory)){
                    echo "Sorry! File is already exists";
                    $fileUpload = false;
                }elseif($_FILES['profile_photo']['size'] > 500000){
                    echo "দুঃখিত, আপনার ফাইলটি অনেক বড় আকারের।";
                    $fileUpload = false;
                }elseif($file_extension != 'jpg' && $file_extension != 'jpeg' && $file_extension != 'png' && $file_extension != 'gif'){
                    echo "দুঃখিত, শুধুমাত্র JPG, JPEG, PNG এবং GIF ফাইল আপলোড করা যাবে।";
                    $fileUpload = false;
                }elseif(!$fileUpload){
                    echo "দুঃখিত, আপনার ফাইলটি আপলোড হয়নি।";
                }else{
                    if(move_uploaded_file($_FILES['profile_photo']['tmp_name'],$file_directory)){
                        echo " ". basename( $_FILES["profile_photo"]["name"]). " ফাইলটি আপলোড হয়েছে। ";
                        echo "<pre>";
                        print_r($check);
                    }else{
                        echo "দুঃখিত, আপনার ফাইলটি আপলোডে সমস্যা হচ্ছে।";
                    }
                }
            }
        }
    ?>
    
<?php include '../../assets/footer.php';
