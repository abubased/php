<?php ob_start();
define("Title","Contact Form");
include '../../assets/header.php';
include '../../assets/headerbottom.php';

echo '<h2 class="text-center">'. Title .':</h2>';?>
    <?php
        // validation errors
        if($_POST){
            $error= "";
        
            //User Inputs
            $name = $_POST['name'];
            $email = $_POST['email'];
            $message = $_POST['message'];
            //Create Error message
            $blankName = '<p><strong>Please fill up your name field !</strong></p>';
            $blankEmail = '<p><strong>Please fill up your Email field !</strong></p>';
            $invalidEmail = '<p><strong>Please enter a valid email address !</strong></p>';
            $blankMessage = '<p><strong>Please fill up your Message field !</strong></p>';

            (!$name)?$error .= $blankName : $name = filter_var($name,FILTER_SANITIZE_STRING);
            (!$email)?$error .= $blankEmail : $email = filter_var($email,FILTER_SANITIZE_EMAIL);
            ($email && !filter_var($email,FILTER_VALIDATE_EMAIL)) ? $error .= $invalidEmail : $email = filter_var($email,FILTER_VALIDATE_EMAIL);
            (!$message)?$error .= $blankMessage : $message = filter_var($message,FILTER_SANITIZE_EMAIL);
        
            // if have any error
            if ($error) {
                $reasultMessage = '<div class="alert alert-danger" role="alert">'.$error.'</div>';
            }else{
                // if no error
                $to = 'abubased@outlook.com';
                $subject = 'Contact';
                $message = '
                <p>Name:'.$name.'</p>
                <p>Email:'.$email.'</p>
                <p>Message:</p>
                <p><strong>'.$message.'</strong></p>  
                ';
                $headers = 'Content-type: text/html';
                // Send the email
                if(mail($to,$subject,$message,$headers)){
                    // if mail send successfully
                    $reasultMessage = '
                    <div class="alert alert-success" role="alert">
                        <p> Email has been successfully send. Please wait until we check your message.Besure we will come back to you as soon as possible <br>Thank you! </p>
                    </div>';
                }else{
                    // if faild to send message
                        // $reasultMessage = '
                        // <div class="alert alert-warning" role="alert">
                        //     <p> Unable to send your email. Please try again later ! </p>
                        // </div>';
                        header('Location:thankyou.php');
                }
            }
            //print reasult message
            echo $reasultMessage;
        }
    ?>
    <form method="post">
        <div class="form-group">
            <label for="name">Name</label>
            <input name="name" type="text" class="form-control" id="name" placeholder="Full Name">
        </div>
        <div class="form-group">
            <label for="email">Email address</label>
            <input name="email" type="email" class="form-control" id="email" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>

        <div class="form-group">
        <label for="name">Your Message</label>
        <textarea class="form-control" name="message" id="message" row="5"></textarea>
        </div>

        <button name="submit" type="submit" class="btn btn-success">Send Message</button>
    </form>
<?php include '../../assets/footer.php'; ?>
<?php ob_flush(); ?>
