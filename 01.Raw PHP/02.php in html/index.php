<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta author="Md Abu Based">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP IN HTML</title>
</head>
<body>
    <h2>Writing Php Code in HTML</h2>
    <p>পিএইচপি ফাইলে HTML লেখা যায় তবে সেটা &lt;?php ?&gt; ট্যাগের বাহিরে লিখতে হবে &lt;?php ?&gt; ট্যাগের ভিতরেও HTML লিখা যায় তবে সেটা কোটেশনের &#34; &#34; ভিতরে লিখতে হবে  ,আবার HTML এর যেকোন স্থানেও পিএইচপি লিখা যায়,সে জন্য &lt;?php দিয়ে পিএইচপি শুরু করে তারপর ইচ্ছাকৃত কোড লিখে শেষে ?&gt; দিয়ে শেষ করতে হয় ।<br>
    উদাহরনঃ<br></p>
    <pre>
    &lt;p&gt;
        &lt;?php
            echo 'this is a pragraph'&#59;
        ?&gt;
    &lt;/p&gt;

    &lt;p&gt;
        &lt;?php
            echo 'this is another pragraph'&#59;
        ?&gt;
    &lt;/p&gt;
    </pre>
    <p>ফলাফলঃ<br>
        <?php
            echo "this is a pragraph";
        ?>
    </p>
    <p>
        <?php
            echo "this is another pragraph";
        ?>
    </p>





    <br><br><hr><center>Copyright by &copy; Md Abu Based</center>
</body>
</html>
