<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta author="Md Abu Based">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data type Strings</title>
</head>
<body>

    <div>
        <p>পিএইচপি এর ডাটা টাইপ গুলোর মধ্যে স্ট্রিং একটি, স্ট্রিং সাধারণত যেকোন ধরনের Text যা &#39; &#39; বা &#34; &#34; এর মধ্যে লিখতে হয় ।
        কখোনো কখোনো ডাবল কোটেশনের মধ্যে আমাদের সিংগেল কোটেশন বা সিংগেল কোটেশনের মধ্যে ডাবল কোটেশন দেওয়ার প্রয়োজন পরে,
        যখন আমদের সিংগেল কোটেশনের প্রয়োজন তখন  সেই স্ট্রিংকে আমরা ডাবল কোটেশনের মধ্যে রেখে স্ট্রিং লিখতে পারি
        উদাহরণ ২ এ লক্ষ্য করলে বিষয়টি ক্লিয়ার হতে পারবেন ।<br>
        উদাহরণঃ
        </p>
        <pre>
            &lt;?php
                &dollar;name = &#39;Rony&#39;&#59;
                echo &#39;Name: &#39;.&dollar;name.&#39;&lt;br/&gt;&#59;
                &dollar;string = &#34;Rony&#39;s have a nice car&#34;
                echo &dollar;string&#39;&#59;
            ?&gt;
        </pre>
        <?php 
            $name = 'Rony';
            echo 'Name: '.$name.'<br />';
            $string = "Rony's have a nice car";
            echo $string;
        ?>
    </div>








    <br><br><hr><center>Copyright by &copy; Md Abu Based</center>
</body>
</html>
