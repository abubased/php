<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>If and Switch statements</title>
</head>
<body>
    <h2>If else and Switch:</h2>
    <?php
        $beliveInYourDreams = false;
        if($beliveInYourDreams){
            echo "<p>Your will be successful!</p>";
        }else{
            echo "<P>Belive in your dreams to be successful in your life</P>";
        }
        $x = ($beliveInYourDreams)?'A':'B';
        echo $x;
        $temperature = 28;
        if($temperature<20){
            echo '<p>It is Cold</p>';
        }elseif($temperature>30){
            echo '<P>It is Hot</P>';
        }else{
            echo '<P>The Temperature is Medium</P>';
        }
        $strength = 'competetion';
        switch($strength){
            case 'belief':
                echo '<p>You have core values that are unchanging,They will define your purpose in life.</p>';
            break;
            case 'communication':
                echo '<p>You easily put your thoughts into words.You are a good presenter.</p>';
            break;
            case 'competetion':
                echo '<p>You measure your progress against the progress of outhers.You strive to win first.</p>';
            break;
            default:
                echo '<p>Choose a strength,you have many for sure!</p>';
            break;

        }

    ?>
</body>
</html>
