<?php define("Title","Data Type Array");
include '../../assets/header.php';
include '../../assets/headerbottom.php';

echo '<h2 class="text-center">'. Title .':</h2>';?>
        
<!-- You can start from here -->

    <?php
        $arrayName = array('tomato','ginjer','onion' );
        print_r($arrayName);
        echo "<br>The second array items is:";
        echo $arrayName[1];

        //associative array
        $shopingbag = array(
            'a' => 'T-shirt',
            'b' => 'Jeans Pant',
            'c' => 'Panjabi'
        );

        echo '<br>';
        print_r($shopingbag);
        var_dump($shopingbag);

        $shopingbag2 = array(
            'b' => 'Jeans Pant',
            'a' => 'T-shirt',
            'c' => 'Panjabi'
        );
        echo '<br>';
        //was they are equal? (==)
        var_dump($shopingbag == $shopingbag2);
        //was they are equal with type? (===)
        var_dump($shopingbag === $shopingbag2);

        $bazzerbag = array(
            'potato' => 'two kilogram',
            'onion' => 'one and half kilogram',
            'oil' => 'one liter'
        );

        echo '<br>';
        //was they are Not equal? (<>)
        var_dump($shopingbag <> $shopingbag2);
        //was they are Not equal? (!=)
        var_dump($shopingbag != $bazzerbag);

        $allbags = $shopingbag + $shopingbag2 + $bazzerbag;
        
        print_r($allbags);
    ?>
    <br><a href="TwoDimentionalArray.php">PHP Super Global Variable</a>
<?php include '../../assets/footer.php';

