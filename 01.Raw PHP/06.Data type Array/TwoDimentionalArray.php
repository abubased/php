<?php define("Title","Multidimention Array");
include '../../assets/header.php';
echo "<style type='text/css'>
   table, th, td{
     border: 2px solid;
     border-collapse: collapse;
     padding: 2px;
   }
   table{
     width: 100%;
   }
 </style>";
include '../../assets/headerbottom.php';

echo '<h2 class="text-center"> '. Title .' :</h2><hr>';?>
        
<!-- You can start from here -->

<?php 
// two dimentional array
    $mobile = [
        ["স্যামসাং",95,53],
        ["নকিয়া",169,113],
        ["আইফোন",70,45],
        ["ব্লাকবেরি",25,10]
    ];

    echo "<u><h2>Array to string a single item:</h2></u>";
// array to string
    $x = implode(" ",$mobile[0]);
    // echo as a string
    echo $x."<br>";
    echo "<u><h2>Array to string each array item using for loop:</h2></u>";
    for($i=0; $i<count($mobile);$i++){
        echo implode(' : ',$mobile[$i])."<br>";
    }
    ?>

    <u><h2>Create a Table With This array</h2></u>
    <table>
        <?php 
            for($tr=0;$tr<count($mobile);$tr++){
                echo "<tr>";
                ?><?php
                    for($td=0; $td<count($mobile[$tr]); $td++){
                        echo "<td>".$mobile[$tr][$td]."</td>";
                    }
                ?>
                <?php echo "</tr>";
            }?>
    </table>

<?php include '../../assets/footer.php';
