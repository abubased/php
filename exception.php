<?php
//এক্সেপশনসহ একটি ফাংশন তৈরি করুন
function checkNumber($num) {
  if($num>1) {
    throw new Exception("Value must be 1 or below");
  }
  return true;
}

//এক্সেপশনকে ট্রিগার করুন
checkNumber(2);
?>
