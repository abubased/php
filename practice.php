<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Image Upload In PHP</title>
</head>
<body>
    <form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data">
        <input type="file" name="img"><br>
        <button type="submit">Upload</button>
    </form>
    <?php
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(!$_FILES['img']['tmp_name']){
                echo "Please Choose an Image";
            }else{
                $location = 'assets/images/';
                $path = $location.basename($_FILES['img']['name']);
                $extension = pathinfo($path,PATHINFO_EXTENSION);
                $status = false;
                $check = getimagesize($_FILES['img']['tmp_name']);
                if($check == false){
                    echo "দুঃখিত ফাইলটি ইমেজ নয়";
                }elseif($extension != 'jpg' && $extension != 'png' &&$extension != 'jpeg' && $extension != 'gif'){
                    echo "দুঃখিত, শুধুমাত্র JPG, JPEG, PNG এবং GIF ফাইল আপলোড করা যাবে।";
                }elseif($_FILES['img']['size'] > 300000){
                    echo "আপনার ফাইলটি অনেক বড়";
                }elseif(file_exists($path)){
                    echo "ফাইলটি আগে থেকেই আছে";
                }else{
                    $status = true;
                }
                if($status && move_uploaded_file($_FILES['img']['tmp_name'],$path)){
                    echo "<h1>".basename($path)." ফাইলটি আপলোড হয়েছে </h1><br>";
                    echo "<h4>&dollar;_FILES এরেটির Key এবং Value সুমহঃ</h4><hr>";
                    echo "<pre>";
                    print_r($_FILES);
                    echo "</pre>";
                    echo "<h4>getimagesize(".basename($path).") এরেটির Key এবং Value সুমহঃ</h4><hr>";
                    echo "<pre>";
                    print_r($check);
                }else{
                    echo "<br>দুঃখিত ফাইলটি আপলোড করা সম্ভব হয়নি ।";
                }
            }
        }
    ?>
</body>
</html>
